package de.spone.workforce.service;

import java.util.Arrays;
import java.util.List;

import org.hamcrest.MatcherAssert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import de.spone.workforce.model.BuildingsAndWorkforce;
import de.spone.workforce.model.WorkforceDistribution;

@RunWith(MockitoJUnitRunner.class)
public class DistributionServiceTest {

	@InjectMocks
	private DistributionService distributionService;

	@Test
	public void testDistribution() {
		BuildingsAndWorkforce buildingsAndWorkforce = new BuildingsAndWorkforce();

		WorkforceDistribution expected1 = new WorkforceDistribution(3, 1);
		WorkforceDistribution expected2 = new WorkforceDistribution(1, 2);
		WorkforceDistribution expected3 = new WorkforceDistribution(1, 1);

		buildingsAndWorkforce.setRooms(Arrays.asList(35, 21, 17));
		buildingsAndWorkforce.setJunior(6);
		buildingsAndWorkforce.setSenior(10);

		List<WorkforceDistribution> result = distributionService.distributeWorkforce(buildingsAndWorkforce);

		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The result must not be empty.", !result.isEmpty());
		MatcherAssert.assertThat("The result must contain three items.", result.size() == 3);
		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The first result must be equal to the expected value.", result.get(0).equals(expected1));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(0).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(0).getJunior() >= 35);		
		MatcherAssert.assertThat("The second result must be equal to the expected value.", result.get(1).equals(expected2));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(1).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(1).getJunior() >= 21);
		MatcherAssert.assertThat("The third result must be equal to the expected value.", result.get(2).equals(expected3));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(2).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(2).getJunior() >= 17);
	}
	
	@Test
	public void testDistribution2() {
		BuildingsAndWorkforce buildingsAndWorkforce = new BuildingsAndWorkforce();

		WorkforceDistribution expected1 = new WorkforceDistribution(2, 1);
		WorkforceDistribution expected2 = new WorkforceDistribution(2, 1);

		buildingsAndWorkforce.setRooms(Arrays.asList(24, 28));
		buildingsAndWorkforce.setJunior(6);
		buildingsAndWorkforce.setSenior(11);

		List<WorkforceDistribution> result = distributionService.distributeWorkforce(buildingsAndWorkforce);

		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The result must not be empty.", !result.isEmpty());
		MatcherAssert.assertThat("The result must contain three items.", result.size() == 2);
		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The first result must be equal to the expected value.", result.get(0).equals(expected1));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(0).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(0).getJunior() >= 24);		
		MatcherAssert.assertThat("The second result must be equal to the expected value.", result.get(1).equals(expected2));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(1).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(1).getJunior() >= 28);
	}
	
	@Test
	public void testDistribution3() {
		BuildingsAndWorkforce buildingsAndWorkforce = new BuildingsAndWorkforce();

		WorkforceDistribution expected1 = new WorkforceDistribution(1, 0);
		WorkforceDistribution expected2 = new WorkforceDistribution(1, 0);

		buildingsAndWorkforce.setRooms(Arrays.asList(6, 11));
		buildingsAndWorkforce.setJunior(6);
		buildingsAndWorkforce.setSenior(11);

		List<WorkforceDistribution> result = distributionService.distributeWorkforce(buildingsAndWorkforce);

		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The result must not be empty.", !result.isEmpty());
		MatcherAssert.assertThat("The result must contain three items.", result.size() == 2);
		MatcherAssert.assertThat("The result must not be null.", result != null);
		MatcherAssert.assertThat("The first result must be equal to the expected value.", result.get(0).equals(expected1));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(0).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(0).getJunior() >= 6);
		MatcherAssert.assertThat("The second result must be equal to the expected value.", result.get(1).equals(expected2));
		MatcherAssert.assertThat("There must be either equal work force or more", DistributionService.SENIOR_WORKFORCE_CAPACITY * result.get(1).getSenior() + DistributionService.JUNIOR_WORKFORCE_CAPACITY * result.get(1).getJunior() >= 11);
	}

}
