package de.spone.workforce.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import de.spone.workforce.model.BuildingsAndWorkforce;
import de.spone.workforce.model.WorkforceDistribution;
import de.spone.workforce.service.DistributionService;

@RestController
public class DistributionController {
	
	@Autowired
	private DistributionService distributionService;
	
	@PostMapping("distribute")
	public List<WorkforceDistribution> distributeWorkforce(@RequestBody BuildingsAndWorkforce buildingsAndWorkforce) {
		return distributionService.distributeWorkforce(buildingsAndWorkforce);
	}
	
}
