package de.spone.workforce.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import de.spone.workforce.model.BuildingsAndWorkforce;
import de.spone.workforce.model.WorkforceDistribution;

@Service
public class DistributionService {
	
	public static final int SENIOR_WORKFORCE_CAPACITY = 11;
	
	public static final int JUNIOR_WORKFORCE_CAPACITY = 7;
	
	public List<WorkforceDistribution> distributeWorkforce(BuildingsAndWorkforce buildingsAndWorkforce) {
		List<WorkforceDistribution> result = new ArrayList<>();
		
		for (Integer rooms: buildingsAndWorkforce.getRooms()) {
			WorkforceDistribution distribution = new WorkforceDistribution();
			if (rooms < SENIOR_WORKFORCE_CAPACITY) {
				// there needs to be at least one senior cleaner.
				distribution.setSenior(1);
			} else {
				// if the workload requires more than just one senior cleaner.
				distribution.setSenior((int)(rooms/SENIOR_WORKFORCE_CAPACITY));
				if (rooms % SENIOR_WORKFORCE_CAPACITY != 0) {
					int rem = rooms % SENIOR_WORKFORCE_CAPACITY;
					if (rem <= JUNIOR_WORKFORCE_CAPACITY) {
						distribution.setJunior(1);
					} else {
						distribution.setJunior(2);
					}
				}
			}
			result.add(distribution);
		}
		
		return result;
	}
	
}
