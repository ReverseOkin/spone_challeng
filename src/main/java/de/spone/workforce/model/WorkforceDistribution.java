package de.spone.workforce.model;

import java.util.Objects;

public class WorkforceDistribution {
	
	private Integer senior = 0;
	
	private Integer junior = 0;
	
	public WorkforceDistribution() {}
	
	public WorkforceDistribution(Integer senior, Integer junior) {
		this.senior = senior;
		this.junior = junior;
	}

	public Integer getSenior() {
		return senior;
	}

	public void setSenior(Integer senior) {
		this.senior = senior;
	}

	public Integer getJunior() {
		return junior;
	}

	public void setJunior(Integer junior) {
		this.junior = junior;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WorkforceDistribution) {
			WorkforceDistribution other = (WorkforceDistribution) obj;
			return senior.equals(other.senior) && junior.equals(other.junior);
		}
		return super.equals(obj);
	}

	@Override
	public int hashCode() {
		return Objects.hash(senior, junior);
	}
	
}
