# Service Partner One Challenge
This is my approach to the service partner one challenge. It exposes a single end point. 

For further documentation on the end point and how to use it please consult the swagger UI while the application is running.
http://localhost:8080/swagger-ui.html#/distribution-controller/distributeWorkforceUsingGET

# How to build?
mvn clean install

# How it works?
First I've approximated the work capacity for a senior(11) and a junior(7) cleaner. The goal is to keep the 
overcapacity at a minimum. This can be achieved by first dividing the workload by the amount of senior staff.
The remainder will be filled up with junior cleaners. The case that there might not be enough cleaning personal
was not part of the assignment.